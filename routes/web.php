<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/login','Auth\loginController@login');

// $router->post('/register','Auth\registerController@register');

$router->post('/logout','Auth\registerController@logout');

// $router->get('/tes','Auth\registerController@tes');

// $router->post('/admin','tesAdminController@index');
// $router->post('/dosen','tesDosenController@index');

//-----------------------------dosen-------------------------------------------------------

//-----------------------------kuis--------------------------------------------------------

$router->get('/dosen/kuis/{id}','DosenController\dosenkuisController@getDataKuis');

// $router->get('/dosen/kuis/update/{id}','DosenController\dosenkuisController@kuisById');

// $router->get('/dosen/kuis/soal/byidkuis/{id}','DosenController\dosenkuisController@soalByIdKuis');

// $router->get('/dosen/kuis/soal/byid/{id}','DosenController\dosenkuisController@soalById');

// $router->get('/dosen/kuis/soal/pilihan/byidsoal/{id}','DosenController\dosenkuisController@pilihanByIdSoal');

// $router->get('/dosen/kuis/soal/pilihan/byid/{id}','DosenController\dosenkuisController@pilihanById');

$router->put('/dosen/kuis/update','DosenController\dosenkuisController@updateKuis');

$router->put('/dosen/kuis/soal/update','DosenController\dosenkuisController@updateSoal');

$router->put('/dosen/kuis/soal/pilihan/update','DosenController\dosenkuisController@updatePilihan');

$router->get('/dosen/kuis/{id}/{search}','DosenController\dosenkuisController@searchDataKuis');

$router->get('/dosen/kuis/soal/detail/{id}','DosenController\dosenkuisController@detailKuis');

// $router->get('/dosen/kuis/soal/detail/gambar/soal/{id}','DosenController\dosenkuisController@getImageSoal');

// $router->get('/dosen/kuis/soal/detail/gambar/pilihan/{id}','DosenController\dosenkuisController@getImagePilihan');

$router->post('/dosen/kuis/add','DosenController\dosenkuisController@addKuis');

$router->post('/dosen/kuis/soal/add','DosenController\dosenkuisController@addSoal');

$router->post('/dosen/kuis/soal/pilihan/add','DosenController\dosenkuisController@addPilihan');

$router->delete('/dosen/kuis/delete/{id}','DosenController\dosenkuisController@deleteKuis');

$router->delete('/dosen/kuis/soal/delete/{id}','DosenController\dosenkuisController@deleteSoal');

$router->delete('/dosen/kuis/soal/pilihan/delete/{id}','DosenController\dosenkuisController@deletePilihan');

// $router->get('tes','DosenController\dosenkuisController@tes');

//---------------------------------------------sesi----------------------------------------------

$router->get('/dosen/sesi/grup/{id}','DosenController\dosenSesiController@grupByIdDosen');

// $router->get('/dosen/sesi/grup/name/{id}/{grup}','DosenController\dosenSesiController@grupByName');

$router->get('/dosen/sesi/grup/{id}/{search}','DosenController\dosenSesiController@searchGrup');

$router->get('/dosen/sesi/grup/detail/{id}/{grup}','DosenController\dosenSesiController@detailGrupSesi');

$router->get('/dosen/sesi/grup/detail/sesi/byid/{id}','DosenController\dosenSesiController@sesiById');

$router->put('/dosen/sesi/grup/detail/update/status','DosenController\dosenSesiController@updateStatusSesi');

$router->put('/dosen/sesi/grup/update/name','DosenController\dosenSesiController@updateGrupSesi');

$router->put('/dosen/sesi/grup/detail/sesi/update','DosenController\dosenSesiController@updateDataSesi');

$router->post('/dosen/sesi/add','DosenController\dosenSesiController@addSesi');

$router->delete('/dosen/sesi/nilai/delete/{id}','DosenController\dosenSesiController@deleteSesi');

//---------------------------------------------nilai----------------------------------------------

$router->get('/dosen/nilai/{id}','DosenController\dosenNilaiController@getDataHasilKuis');

$router->get('/dosen/nilai/all/grup/{id}/{grup}','DosenController\dosenNilaiController@getSemuaDataHasilKuis');

//-----------------------------mahasiswa-------------------------------------------------------

$router->get('/mahasiswa/tes/soal/kuis/{kode}','MahasiswaController\mahasiswaController@tesSoalKuis');

$router->get('/mahasiswa/nilai/{nim}','MahasiswaController\mahasiswaController@getDataHasilNilaiMahasiswa');

$router->get('/mahasiswa/nilai/cek/{kode}/{nim}','MahasiswaController\mahasiswaController@getDataHasilNilaiMahasiswaByKode');

$router->get('/mahasiswa/grup/{nim}','MahasiswaController\mahasiswaController@grupByNim');

$router->get('/mahasiswa/nilai/{nim}/{grup}','MahasiswaController\mahasiswaController@getDataHasilNilaiMahasiswaByGrup');

$router->post('/mahasiswa/tes/soal/kuis/nilai/add','MahasiswaController\mahasiswaController@addDataHasilKuis');

$router->get('/mahasiswa/tes/soal/kuis/kondisi/pilihan/{id}','MahasiswaController\mahasiswaController@getKondisiPilihanById');