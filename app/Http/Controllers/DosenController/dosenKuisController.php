<?php

namespace App\Http\Controllers\DosenController;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Models\Kuis;
use App\Models\Sesi;
use App\Models\Soal;
use App\Models\Pilihan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use Illuminate\Database\QueryException;

class dosenKuisController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('dosen');
    }

    public function getDataKuis($id)
    {
        $kuis = Kuis::select('id','name')
        ->where('dosen_id',$id)->orderBy('id', 'desc')->get();

        $response =
                [
                    'message' => 'Sukses',
                    'data' => $kuis
                ];

            return response()->json($response, 200);
    }

    public function searchDataKuis($id,$search)
    {
        $data = urldecode($search);
        $kuis = Kuis::select('id','name')
        ->where('dosen_id',$id)
        ->where('name', 'like', "%{$data}%")->get();

        $response =
                [
                    'message' => 'Sukses',
                    'data' => $kuis
                ];

            return response()->json($response, 200);
    }

    public function detailKuis($id)
    {
        $detail = DB::table('datasoal')
        ->select('datasoal.soal', 'datasoal.id as id_soal')
        ->join('kuis','datasoal.kuis_id','=','kuis.id')
        ->where('kuis.id',$id)->orderBy('id_soal', 'desc')->get();

        $jumsoal = $detail->count();

        if($jumsoal==0)
        {
            $response =
            [
                'message' => 'Data Soal Kosong',
                'id_kuis' => $id,
                'data' => []
            ];

            return response()->json($response, 404); //Not Found

        }
          
        for ($x = 0; $x < $jumsoal; $x++) {

            $pilihan = DB::table('datasoal')
            ->select('pilihanganda.id as id_jawaban','pilihanganda.jawaban','pilihanganda.kondisi',)
            ->join('kuis','datasoal.kuis_id','=','kuis.id')
            ->join('pilihanganda','pilihanganda.soal_id','=','datasoal.id')
            ->where('datasoal.id',$detail[0+$x]->id_soal)->get();

            $getsoal[$x] = [$detail[0+$x],$pilihan];
        } 
        
        $response =
                [
                    'message' => 'Sukses',
                    'id_kuis' => $id,
                    'data' => $getsoal
                ];

                return response()->json($response, 200);
    }

    public function updateKuis(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),422);
        }

        try{        
            $kuis = Kuis::where('id',$request->id)->update([
                'name'=>$request->name,
            ]);

             $response =
                [
                    'message' => 'Update Sukses'
                ];

            return response()->json($response, 200);

        }catch(QueryException $e){

            return response()->json([
                'message' => $e->getMessage()
            ],500);

        }
       
    }

    public function addKuis(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),422);
        }

        try{        
            $kuis = Kuis::create([
                'name'=>$request->name,
                'dosen_id'=>request()->user()->id,
            ]);

            $response =
                [
                    'message' => 'Tambah Data Sukses',
                    'data' => $kuis
                ];

            return response()->json($response, 200);

        }catch(QueryException $e){

            return response()->json([
                'message' => $e->getMessage()
            ],500);

        }
       
    }

    public function addSoal(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'kuis_id' => 'required',
            'soal' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),422);
        }

        try{ 
            // $image = $request->file('image');
            // $namaimg = time().'.'.$image->getClientOriginalExtension();
            // $image->move('images',$namaimg);
      
            $soal = Soal::create([
                'kuis_id'=>$request->kuis_id,
                'soal'=>$request->soal,
            ]);

            $response =
                [
                    'message' => 'Tambah Data Sukses',
                    'data' => $soal
                ];

            return response()->json($response, 200);

        }catch(QueryException $e){

            return response()->json([
                'message' => $e->getMessage()
            ],500);

        }
       
    }

    public function updateSoal(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'soal' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),422);
        }

        try{        
            $soal = Soal::where('id',$request->id)->update([
                'soal'=>$request->soal,
            ]);

             $response =
                [
                    'message' => 'Update Sukses'
                ];

            return response()->json($response, 200);

        }catch(QueryException $e){

            return response()->json([
                'message' => $e->getMessage()
            ],500);

        }
       
    }

    public function addPilihan(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'soal_id' => 'required',
            'jawaban' => 'required',
            'kondisi' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),422);
        }

        try{
            // $image = $request->file('image');
            // $namaimg = time().'.'.$image->getClientOriginalExtension();
            // $image->move('images',$namaimg);
                    
            $pilihan = Pilihan::create([
                'soal_id'=>$request->soal_id,
                'jawaban'=>$request->jawaban,
                'kondisi'=>$request->kondisi,
            ]);

             $response =
                [
                    'message' => 'Tambah Data Sukses',
                    'data' => $pilihan
                ];

            return response()->json($response, 200);

        }catch(QueryException $e){

            return response()->json([
                'message' => $e->getMessage()
            ],500);

        }
       
    }

    public function updatePilihan(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'jawaban' => 'required',
            'kondisi' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),422);
        }

        try{        
            $pilihan = Pilihan::where('id',$request->id)->update([
                'jawaban'=>$request->jawaban,
                'kondisi'=>$request->kondisi,
            ]);

             $response =
                [
                    'message' => 'Update Sukses'
                ];

            return response()->json($response, 200);

        }catch(QueryException $e){

            return response()->json([
                'message' => $e->getMessage()
            ],500);

        }
       
    }

    public function deletePilihan($id)
    {
        try{        
            $pilihan = Pilihan::where('id',$id)->delete();

             $response =
                [
                    'message' => 'Delete Sukses'
                ];

            return response()->json($response, 200);

        }catch(QueryException $e){

            return response()->json([
                'message' => $e->getMessage()
            ],500);

        }
       
    }

    public function deleteSoal($id)
    {
        try{

            Pilihan::where('soal_id',$id)->delete();

            Soal::where('id',$id)->delete();

             $response =
                [
                    'message' => 'Delete Sukses',
                ];

            return response()->json($response, 200);

        }catch(QueryException $e){

            return response()->json([
                'message' => $e->getMessage()
            ],500);

        }
       
    }

    public function deleteKuis($id)
    {

        $sesi = Sesi::select('id')->where('kuis_id',$id)->first();

        if($sesi != null)
        {
            $response =
            [
                'message' => 'Delete Gagal, Kuis Ini Sedang Digunakan',
                'status' => '0'
            ];

        return response()->json($response, 422);
        }
        

        try{
            $soal_id = Soal::select('id')->where('kuis_id',$id)->get();

            if($soal_id != null)
            {
                for($x = 0; $x < count($soal_id); $x++)
                {
                    $pilihan_id = Pilihan::select('id')->where('soal_id',$soal_id[$x]['id'])->first();
                    
                    if($pilihan_id != null)
                    {
                        Pilihan::where('soal_id',$soal_id[$x]['id'])->delete();
                    }
                }

                Soal::where('kuis_id',$id)->delete();

                Kuis::where('id',$id)->delete();
    
                        $response =
                            [
                                'message' => 'Delete Sukses',
                                'status' => '1'
                            ];
    
                        return response()->json($response, 200); 
            }
            
            Kuis::where('id',$id)->delete();

                    $response =
                        [
                            'message' => 'Delete Sukses',
                            'status' => '1'
                        ];

                    return response()->json($response, 200);

        }catch(QueryException $e){

            return response()->json([
                'message' => $e->getMessage(),
                'status' => '0'
            ],500);

        }
    }

    // public function kuisById($id)
    // {
    //     $kuis = Kuis::select('id','name')
    //     ->where('id',$id)->first();

    //     $response =
    //             [
    //                 'message' => 'Sukses',
    //                 'data' => $kuis
    //             ];

    //         return response()->json($response, 200);
    // }

    // public function pilihanByIdSoal($id)
    // {
    //     $soal = Soal::select('id')
    //     ->where('id',$id)->first();

    //     $response =
    //             [
    //                 'message' => 'Sukses',
    //                 'data' => $soal
    //             ];

    //         return response()->json($response, 200);
    // }

    // public function pilihanById($id)
    // {
    //     $pilihan = Pilihan::select('id', 'jawaban', 'kondisi')
    //     ->where('id',$id)->first();

    //     $response =
    //             [
    //                 'message' => 'Sukses',
    //                 'data' => $pilihan
    //             ];

    //         return response()->json($response, 200);
    // }

    // public function soalByIdKuis($id)
    // {
    //     $kuis = Kuis::select('id')
    //     ->where('id',$id)->first();

    //     $response =
    //             [
    //                 'message' => 'Sukses',
    //                 'data' => $kuis
    //             ];

    //         return response()->json($response, 200);
    // }

    // public function soalById($id)
    // {
    //     $soal = Soal::select('id', 'soal')
    //     ->where('id',$id)->first();

    //     $response =
    //             [
    //                 'message' => 'Sukses',
    //                 'data' => $soal
    //             ];

    //         return response()->json($response, 200);
    // }
}
