<?php

namespace App\Http\Controllers\DosenController;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Models\Kuis;
use App\Models\Sesi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class dosenNilaiController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('dosen');
    }

    public function getDataHasilKuis($id)
    {
        $data = DB::table('nilaimahasiswa')
        ->select('nilaimahasiswa.NIM', 'nilaimahasiswa.name as nama_mahasiswa', 
        'sesi.name as nama_sesi', 'nilaimahasiswa.skor',)
        ->join('sesi','sesi.id','=','nilaimahasiswa.sesi_id')
        ->where('sesi_id', $id)
        ->get();

        if(!$data)
        {
            $response =
                [
                    'message' => 'Data Nilai Kosong',
                    'data' => []
                ];

                return response()->json($response, 404);
        }

        $response =
                [
                    'message' => 'Sukses',
                    'data' => $data
                ];

                return response()->json($response, 200);

    }

    public function getSemuaDataHasilKuis($id,$grup)
    {
        $data = DB::table('nilaimahasiswa')
        ->select('nilaimahasiswa.NIM')
        ->join('sesi','sesi.id','=','nilaimahasiswa.sesi_id')
        ->join('kuis','sesi.kuis_id','=','kuis.id')
        ->where('sesi.grup', urldecode($grup))
        ->where('kuis.dosen_id', $id)
        ->groupBy('nilaimahasiswa.NIM')
        ->orderBy('nilaimahasiswa.NIM', 'asc')
        ->get();

        $jummhs = $data->count();

        if($jummhs==0)
        {
            $response =
                [
                    'message' => 'Data Nilai Kosong',
                    'data' => []
                ];

                return response()->json($response, 404);
        }

        $sesi = DB::table('sesi')
        ->select('sesi.id','sesi.name as nama_sesi')
        ->join('kuis','sesi.kuis_id','=','kuis.id')
        ->where('sesi.grup', urldecode($grup))
        ->where('kuis.dosen_id', $id)
        ->get();

        $jumsesi = $sesi->count();

        for ($x = 0; $x < $jummhs; $x++) {

            for ($j = 0; $j < $jumsesi; $j++) {

                $skor = DB::table('nilaimahasiswa')
                ->select('nilaimahasiswa.skor')
                ->join('sesi','sesi.id','=','nilaimahasiswa.sesi_id')
                ->join('kuis','sesi.kuis_id','=','kuis.id')
                ->where('sesi.id', $sesi[0+$j]->id)
                ->where('nilaimahasiswa.NIM', $data[0+$x]->NIM)
                ->first();

                if($skor == null){

                    $datanilai[$j] = 0;

                }else{

                    $datanilai[$j] = $skor->skor;  

                }
            }

            $alldata[$x] = ['datamhs'=>$data[0+$x], 'skor'=>$datanilai, 'skorakhir'=>(array_sum($datanilai)/$jumsesi)];         
        }

        

        $response =
                [
                    'message' => 'Sukses',
                    'data' => $alldata,
                    'sesi' => $sesi
                ];

                return response()->json($response, 200);

    }

}