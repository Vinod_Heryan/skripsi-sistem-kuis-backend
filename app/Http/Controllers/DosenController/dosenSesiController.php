<?php

namespace App\Http\Controllers\DosenController;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Models\Kuis;
use App\Models\Sesi;
use App\Models\Nilai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Database\QueryException;

class dosenSesiController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('dosen');
    }
    
    public function grupByIdDosen($id)
    {
        $grup = DB::table('sesi')
        ->select('sesi.grup as grup')
        ->join('kuis','sesi.kuis_id','=','kuis.id')
        ->where('kuis.dosen_id',$id)->groupBy('grup')->orderBy('sesi.id', 'desc')->get();

        $response =
                [
                    'message' => 'Sukses',
                    'data' => $grup
                ];

            return response()->json($response, 200);
    }

    public function sesiById($id)
    {
        $sesi = DB::table('sesi')
        ->select('sesi.id', 'kuis.id as kuis_id', 'sesi.name as nama_sesi', 
        'kuis.name as nama_kuis')
        ->join('kuis','sesi.kuis_id','=','kuis.id')
        ->where('sesi.id', $id)->first();

        $response =
                [
                    'message' => 'Sukses',
                    'data' => $sesi
                ];

            return response()->json($response, 200);
    }

    public function searchGrup($id,$search)
    {
        $data = urldecode($search);
        $grup = DB::table('sesi')
        ->select('sesi.grup as grup')
        ->join('kuis','sesi.kuis_id','=','kuis.id')
        ->where('kuis.dosen_id',$id)
        ->where('grup', 'like', "%{$data}%")
        ->groupBy('grup')->get();

        $response =
                [
                    'message' => 'Sukses',
                    'data' => $grup
                ];

            return response()->json($response, 200);
    }

    public function detailGrupSesi($id,$grup)
    {
        $sesi = DB::table('sesi')
        ->select('sesi.id', 'kuis.id as kuis_id', 'kuis.dosen_id', 'sesi.name as nama_sesi', 
        'kuis.name as nama_kuis', 'sesi.kode', 'sesi.status',)
        ->join('kuis','sesi.kuis_id','=','kuis.id')
        ->where('sesi.grup', urldecode($grup))
        ->where('kuis.dosen_id', $id)
        ->get();

        $response =
                [
                    'message' => 'Sukses',
                    'data' => $sesi
                ];

            return response()->json($response, 200);
    }

    public function addSesi(Request $request)
    {

        if($request->grup == null || $request->grupbaru == null)
        {
            if($request->grupbaru == null)
            {
                $validator = Validator::make($request->all(),[
                    'name' => 'required',
                    'kuis_id' => 'required',
                    'grup' => 'required',
                ]);        

            }else if($request->grup == null){

                $validator = Validator::make($request->all(),[
                    'name' => 'required',
                    'kuis_id' => 'required',
                    'grupbaru' => 'required',
                ]);       

            }

            if($validator->fails()){
                return response()->json($validator->errors(),422);
            }

            try{  
                
                if($request->grup == null)
                {
                    $sesi = Sesi::create([
                        'name' => $request->name,
                        'kuis_id' => $request->kuis_id,
                        'grup' => $request->grupbaru,
                        'kode' => Str::random(10),
                        'status' => 1,
                    ]);
        
                     $response =
                        [
                            'message' => 'Tambah Data Sukses',
                            'data' => $sesi
                        ];
        
                    return response()->json($response, 200);

                }else if($request->grupbaru == null){

                    $sesi = Sesi::create([
                        'name' => $request->name,
                        'kuis_id' => $request->kuis_id,
                        'grup' => $request->grup,
                        'kode' => Str::random(10),
                        'status' => 1,
                    ]);
        
                     $response =
                        [
                            'message' => 'Tambah Data Sukses',
                            'data' => $sesi
                        ];
        
                    return response()->json($response, 200);

                }
                
    
            }catch(QueryException $e){
    
                return response()->json([
                    'massage' => $e->getMessage()
                ],500);
    
            }

        }

        $response =
            [
                'message' => 'Tambah Data Gagal Isi Salah Satu Saja Dari Inputan Grup Dan GrupBaru'
            ];

        return response()->json($response,422);  
       
    }

    public function updateGrupSesi(Request $request)
    {
        try{
            $sesi = DB::table('sesi')
            ->join('kuis','sesi.kuis_id','=','kuis.id')
            ->where('kuis.dosen_id', $request->dosen_id)->where('sesi.grup', $request->grup)->update([
                'grup'=> $request->grupbaru,
            ]);        

             $response =
                [
                    'message' => 'Update Grup Sesi Sukses'
                ];

            return response()->json($response, 200);

        }catch(QueryException $e){

            return response()->json([
                'massage' => $e->getMessage()
            ],500);

        }
       
    }

    public function updateStatusSesi(Request $request)
    {
        try{
            if($request->status == 1){

                $sesi = Sesi::where('id', $request->id)->update([
                    'status'=> '0',
                ]);

            }elseif($request->status == 0){

                $sesi = Sesi::where('id', $request->id)->update([
                    'status'=> '1',
                ]);

            }        

             $response =
                [
                    'message' => 'Update Status Sesi Sukses'
                ];

            return response()->json($response, 200);

        }catch(QueryException $e){

            return response()->json([
                'massage' => $e->getMessage()
            ],500);

        }
       
    }

    public function updateDataSesi(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'kuis_id' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),422);
        }

        try{
            $sesi = Sesi::where('id', $request->id)->update([
                'name'=> $request->name,
                'kuis_id'=> $request->kuis_id,
            ]);

             $response =
                [
                    'message' => 'Update Sesi Sukses'
                ];

            return response()->json($response, 200);

        }catch(QueryException $e){

            return response()->json([
                'massage' => $e->getMessage()
            ],500);

        }
       
    }

    public function getDataHasilKuis($kode, $nim)
    {
        $data = $soal = DB::table('nilaimahasiswa')
        ->select('nilaimahasiswa.NIM', 'nilaimahasiswa.name as nama_mahasiswa', 
        'sesi.name as nama_sesi', 'nilaimahasiswa.skor',)
        ->join('sesi','sesi.id','=','nilaimahasiswa.sesi_id')
        ->where('sesi.kode', $kode)
        ->where('nilaimahasiswa.NIM', $nim)
        ->get();

        if(!$data)
        {
            $response =
                [
                    'message' => 'Data Nilai Kosong',
                    'data' => []
                ];

                return response()->json($response, 404);
        }

        $response =
                [
                    'message' => 'Sukses',
                    'data' => $data
                ];

                return response()->json($response, 200);

    }

    public function deleteSesi($id)
    {
        try{

            Nilai::where('sesi_id',$id)->delete();

            Sesi::where('id',$id)->delete();

             $response =
                [
                    'message' => 'Delete Sukses'
                ];

            return response()->json($response, 200);

        }catch(QueryException $e){

            return response()->json([
                'massage' => $e->getMessage()
            ],500);

        }
       
    }

    // public function grupByName($id,$grup)
    // {
    //     $grup = DB::table('sesi')
    //     ->select('sesi.id', 'sesi.grup as grup')
    //     ->join('kuis','sesi.kuis_id','=','kuis.id')
    //     ->where('sesi.grup', urldecode($grup))
    //     ->where('kuis.dosen_id',$id)->first();

    //     $response =
    //             [
    //                 'message' => 'Sukses',
    //                 'data' => $grup,
    //                 'code' => '200'
    //             ];

    //         return response()->json($response, 200);
    // }
}
