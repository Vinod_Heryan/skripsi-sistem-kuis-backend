<?php

namespace App\Http\Controllers;

use App\Models\User;

class tesDosenController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('dosen');
    }

    public function index()
    {
        $data = User::where('is_admin','1')->get();
        $response =
                [
                    'data' => $data,
                    'code' => '200'
                ];
            return response()->json($response, 200);
    }
}
