<?php

namespace App\Http\Controllers;

use App\Models\User;

class tesAdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        $data = User::where('is_admin','0')->get();
        $response =
                [
                    'data' => $data,
                    'code' => '200'
                ];
            return response()->json($response, 200);
    }
}
