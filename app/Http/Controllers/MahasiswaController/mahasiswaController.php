<?php

namespace App\Http\Controllers\MahasiswaController;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Nilai;
use App\Models\Pilihan;

class mahasiswaController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('mahasiswa');
    }

    public function tesSoalKuis($kode)
    {
        $soal = DB::table('datasoal')
        ->select('sesi.id as id_sesi','datasoal.soal', 'datasoal.id as id_soal', 'sesi.status')
        ->join('kuis','datasoal.kuis_id','=','kuis.id')
        ->join('sesi','sesi.kuis_id','=','kuis.id')
        ->where('sesi.kode',$kode)->paginate(1);

        $jumsoal = $soal->count();

        if($jumsoal==0)
        {
            $response =
            [
                'message' => 'Data Soal Kosong',
                'kode_sesi' => $kode,
                'data' => []
            ];

            return response()->json($response, 404);

        }
          
        for ($x = 0; $x < $jumsoal; $x++) {

            $pilihan = DB::table('datasoal')
            ->select('pilihanganda.id as id_jawaban','pilihanganda.jawaban','pilihanganda.kondisi',)
            ->join('kuis','datasoal.kuis_id','=','kuis.id')
            ->join('pilihanganda','pilihanganda.soal_id','=','datasoal.id')
            ->where('datasoal.id',$soal[0+$x]->id_soal)->inRandomOrder()->get();
        }
        
        $response =
                [
                    'message' => 'Sukses',
                    'kode_sesi' => $kode,
                    'data' => $soal,
                    'pilihan' => $pilihan
                ];

                return response()->json($response, 200);
    }

    public function addDataHasilKuis(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'sesi_id' => 'required',
            'NIM' => 'required',
            'name' => 'required',
            'skor' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),422);
        }

        try{        
            $nilai = Nilai::create([
                'sesi_id' => $request->sesi_id,
                'NIM' => $request->NIM,
                'name' => $request->name,
                'skor' => $request->skor,
            ]);

             $response =
                [
                    'message' => 'Tambah Data Sukses',
                    'data' => $nilai
                ];

            return response()->json($response, 200);

        }catch(QueryException $e){

            return response()->json([
                'massage' => 'Failed' . $e->errorInfo
            ]);

        }
    }

    public function getDataHasilNilaiMahasiswa($nim)
    {
        $data = DB::table('nilaimahasiswa')
        ->select('nilaimahasiswa.NIM', 'nilaimahasiswa.name as nama_mahasiswa', 
        'sesi.name as nama_sesi', 'sesi.grup as nama_grup', 'nilaimahasiswa.skor',)
        ->join('sesi','sesi.id','=','nilaimahasiswa.sesi_id')
        ->where('nilaimahasiswa.NIM', $nim)
        ->get();

        if(!$data)
        {
            $response =
                [
                    'message' => 'Data Nilai Kosong',
                    'data' => []
                ];

                return response()->json($response, 404);
        }

        $response =
                [
                    'message' => 'Sukses',
                    'data' => $data
                ];

                return response()->json($response, 200);

    }

    public function grupByNim($nim)
    {
        $data = DB::table('nilaimahasiswa')
        ->select('sesi.grup as nama_grup')
        ->join('sesi','sesi.id','=','nilaimahasiswa.sesi_id')
        ->where('nilaimahasiswa.NIM', $nim)
        ->groupBy('nama_grup')
        ->get();

        if(!$data)
        {
            $response =
                [
                    'message' => 'Data Nilai Kosong',
                    'data' => []
                ];

                return response()->json($response, 404);
        }

        $response =
                [
                    'message' => 'Sukses',
                    'data' => $data
                ];

                return response()->json($response, 200);

    }

    public function getDataHasilNilaiMahasiswaByGrup($nim,$grup)
    {
        $data = DB::table('nilaimahasiswa')
        ->select('nilaimahasiswa.NIM', 'nilaimahasiswa.name as nama_mahasiswa', 
        'sesi.name as nama_sesi', 'sesi.grup as nama_grup', 'nilaimahasiswa.skor',)
        ->join('sesi','sesi.id','=','nilaimahasiswa.sesi_id')
        ->where('nilaimahasiswa.NIM', $nim)
        ->where('sesi.grup', urldecode($grup))
        ->get();

        if(!$data)
        {
            $response =
                [
                    'message' => 'Data Nilai Kosong',
                    'data' => []
                ];

                return response()->json($response, 404);
        }

        $response =
                [
                    'message' => 'Sukses',
                    'data' => $data
                ];

                return response()->json($response, 200);

    }

    public function getDataHasilNilaiMahasiswaByKode($nim,$kode)
    {
        $data = DB::table('nilaimahasiswa')
        ->select('nilaimahasiswa.NIM', 'nilaimahasiswa.name as nama_mahasiswa', 
        'sesi.name as nama_sesi', 'sesi.grup as nama_grup', 'nilaimahasiswa.skor',)
        ->join('sesi','sesi.id','=','nilaimahasiswa.sesi_id')
        ->where('sesi.kode', $kode)
        ->where('nilaimahasiswa.NIM', $nim)
        ->get();

        if(!$data)
        {
            $response =
                [
                    'message' => 'Data Nilai Kosong',
                    'data' => []
                ];

                return response()->json($response, 404);
        }

        $response =
                [
                    'message' => 'Sukses',
                    'data' => $data
                ];

                return response()->json($response, 200);

    }

    public function getKondisiPilihanById($id)
    {
        $data = Pilihan::select('id','kondisi')
        ->where('id',$id)->first();;

        $response =
                [
                    'message' => 'Sukses',
                    'data' => $data
                ];

            return response()->json($response, 200);
    }
}
