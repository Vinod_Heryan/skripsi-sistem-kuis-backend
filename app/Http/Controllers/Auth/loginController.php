<?php

namespace App\Http\Controllers\Auth;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class loginController extends BaseController
{
    public function login(Request $request)
    {
        $user = User::where('no_induk', $request->no_induk)->first();
        if (!$user) {

            $response =
                [
                    'message' => 'Nomor Induk Tidak Valid'
                ];
            return response()->json($response, 401); //Unauthorized 
        }

        if (Hash::check($request->password, $user->password)) {
            $token = Hash::make(date('Y-m-d').$user->no_induk);
            $user->update(['remember_token' => $token]);

            $response =
                [
                    'message' => 'Login Berhasil',
                    'id' => $user->id,
                    'no_induk' => $user->no_induk,
                    'name' => $user->name,
                    'is_admin' => $user->is_admin,
                    'token' => $user->remember_token
                ];
            return response()->json($response, 200);
        }
        
        $response =
            [
                'message' => 'Password Tidak Valid'
            ];
        return response()->json($response, 401); //Unauthorized 
    }

    public function logout()
    {
        $user = request()->user()->all();
        $user->update(['remember_token' => null]);
        Session::truncate();

           $response =
            [
                'message' => 'Logout Berhasil'
            ];
        return response()->json($response, 200);
    }

}
