<?php

namespace App\Http\Controllers\Auth;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Session;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class registerController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'is_admin' => 'required',
            'password' => 'required|min:8',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),422);
        }
        try{        
            $data = User::create([
                'name' => $request->input("name"),
                'email' => $request->input("email"),
                'is_admin' => $request->input("is_admin"),
                'password' => Hash::make($request->input("password")),
            ]);
            $response = 
            [
                'massage' => 'Sukses input',
                'data' => $data,
                'status_code' => 201
            ];
            return response()->json($response,201);

        }catch(QueryException $e){

            return response()->json([
                'massage' => 'Failed' . $e->errorInfo
            ]);

        }
    }

    public function logout()
    {
        $user = request()->user();
        $user->update(['remember_token' => null]);
        Session::truncate();

        return response()->json([
               'message' => "Logout Berhasil",
               'status_code' => 200
           ], 200);
    }

    public function tes()
    {
        $user = request()->user()->name;
        
        return response()->json([
               'message' => $user,
               'status_code' => 200
           ], 200);
    }

    
}
