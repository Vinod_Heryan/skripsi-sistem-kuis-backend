<?php

namespace App\Http\Middleware;

use Closure;

class IsMahasiswa
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(request()->user()->is_admin == 0){
            return $next($request);
        }else{
            $response =
            [
                'massage' => 'Anda Tidak Memiliki Hak Akses Halaman Tersebut..!!!',
                'status_user' => false
            ]; 
            return redirect()->back()->response()->json($response, 403);
        }
    }
}
