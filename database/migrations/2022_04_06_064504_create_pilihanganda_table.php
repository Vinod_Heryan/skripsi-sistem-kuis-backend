<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePilihangandaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pilihanganda', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('soal_id');
            $table->foreign('soal_id')->references('id')->on('datasoal');
            $table->string('jawaban');
            $table->enum('kondisi',['1','0']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pilihanganda');
    }
}
