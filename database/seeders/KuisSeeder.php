<?php

namespace Database\Seeders;

use App\Models\Kuis;
use Illuminate\Database\Seeder;

class KuisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kuis = [
                [
                'dosen_id' => "2",  
                'name' => "DFD",
                ],
                [
                'dosen_id' => "2",  
                'name' => "SFD",
                ],
                [
                'dosen_id' => "2",  
                'name' => "Gerbang Logika",
                ],
                [
                'dosen_id' => "2",
                'name' => "Kuis Perulangan",
                ],
                [
                'dosen_id' => "2",
                'name' => "Kuis Pewarisan",
                ],
        
        ];
        foreach($kuis as $key => $value){
            Kuis::create($value);
        }
    }
}
