<?php

namespace Database\Seeders;

use App\Models\Nilai;
use Illuminate\Database\Seeder;

class NilaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nilai = [
            [
                'sesi_id' => "22",
                'NIM' => "1842101641",
                'name' => "Vinod Heryan",
                'skor' => "60",
                ],
                [
                'sesi_id' => "23",
                'NIM' => "1842101641",
                'name' => "Vinod Heryan",
                'skor' => "80",
                ],

                [
                'sesi_id' => "22",
                'NIM' => "1842101655",
                'name' => "Mujurius Gea",
                'skor' => "60",
                ],
                [
                'sesi_id' => "23",
                'NIM' => "1842101655",
                'name' => "Mujurius Gea",
                'skor' => "80",
                ],

                    
                    [
                    'sesi_id' => "22",
                    'NIM' => "1842101695",
                    'name' => "Reza Fauzi",
                    'skor' => "60",
                    ],
                    [
                    'sesi_id' => "23",
                    'NIM' => "1842101695",
                    'name' => "Reza Fauzi",
                    'skor' => "60",
                    ],
                
                    [
                    'sesi_id' => "22",
                    'NIM' => "1842101600",
                    'name' => "Miquel",
                    'skor' => "60",
                    ],
                    [
                    'sesi_id' => "23",
                    'NIM' => "1842101600",
                    'name' => "Miquel",
                    'skor' => "80",
                    ],
                    

                    [
                    'sesi_id' => "22",
                    'NIM' => "1842101601",
                    'name' => "Tomas",
                    'skor' => "60",
                    ],
                    [
                    'sesi_id' => "23",
                    'NIM' => "1842101601",
                    'name' => "Tomas",
                    'skor' => "60",
                    ],
        ];
        foreach($nilai as $key => $value){
            Nilai::create($value);
        }
    }
}
