<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'no_induk' => "0000000000",
                'name' => "Mahasiswa",
                'email' => 'mahasiswa@gmail.com',
                'is_admin' => "0",
                'password' => Hash::make('12345678'),
            ],

            [
                'no_induk' => "1111111111",    
                'name' => "dosen",
                'email' => 'dosen@gmail.com',
                'is_admin' => "1",
                'password' => Hash::make('12345678'),
            ],
        ];
        foreach($user as $key => $value){
            User::create($value);
        }
    }
}
